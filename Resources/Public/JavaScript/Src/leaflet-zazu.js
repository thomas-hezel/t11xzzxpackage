/* zazu.berlin – die Schwarzwald Werbeagentur in Berlin (c) JavaScript 20190509
author: Thomas Hezel, Berlin
Leaflet Map JavaScript - variable lati and longi coming from FLUID
*/

jQuery(document).ready(function ($) {

    //in jquery click call for leaflet js
    $('#activateMapBtn, #map').click(function () {

        var markerOrt1 = L.icon({
            iconUrl: 'typo3conf/ext/xzzxpackage/Resources/Public/Icons/leaf-green.png',
            shadowUrl: 'typo3conf/ext/xzzxpackage/Resources/Public/Icons/leaf-shadow.png',

            iconSize: [38, 95], // size of the icon
            shadowSize: [38, 95], // size of the shadow
            iconAnchor: [19, 95], // point of the icon which will correspond to marker's location
            shadowAnchor: [0, 95],  // the same for the shadow
            popupAnchor: [-6, -100] // point from which the popup should open relative to the iconAnchor
        });

        var markerOrt2 = L.icon({
            iconUrl: 'typo3conf/ext/xzzxpackage/Resources/Public/Icons/leaf-red.png',
            shadowUrl: 'typo3conf/ext/xzzxpackage/Resources/Public/Icons/leaf-shadow.png',

            iconSize: [38, 95], // size of the icon
            shadowSize: [38, 95], // size of the shadow
            iconAnchor: [19, 95], // point of the icon which will correspond to marker's location
            shadowAnchor: [0, 95],  // the same for the shadow
            popupAnchor: [-6, -100] // point from which the popup should open relative to the iconAnchor
        });

        /***
        * last figure here is giving a zoomfactor: 5 is aprox. Europe, 10 is aprox. a region
        ***/
        var map = L.map('map', { zoomControl: false }).setView([lati, longi], 6);

        /*above remove control and put a new one on bottomleft*/
        map.addControl(L.control.zoom({ position: 'bottomleft' }));

        /*tiles from mapBox
            L.tileLayer('https://{s}.tiles.mapbox.com/v3/uhradone.ija63bia/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18
        }).addTo(map);
        */

        /*tiles from openStreetMap*/
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a>',
            maxZoom: 18
        }).addTo(map);

        L.marker([lati1, longi1], { icon: markerOrt1 }).addTo(map).bindPopup("zazu.berlin<br /><b>digital + film</b>").openPopup();
        L.marker([lati2, longi2], { icon: markerOrt2 }).addTo(map).bindPopup("zazu.winzeln<br /><b>grafik + print</b>").openPopup();

        //jquery  additions
        $('.dataWarning').addClass('hidden');
        $('#map').addClass('dontShowBackground');


        //on click function
    });


});
