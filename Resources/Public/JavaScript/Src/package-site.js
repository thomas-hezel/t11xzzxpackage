/* zazu.berlin – die Schwarzwald Werbeagentur (c) JavaScript 20190509
author: Thomas Hezel, Berlin
main site JS for responsive Design 4col
*/

// Avoid `console` errors in browsers that lack a console. From Boilerplate.
(function () {
    var method;
    var noop = function () { };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


/*
*
*  sitepackage site js inside JQuery
*
*/
$(document).ready(function () {



    /*==== cookies ======
    $('#tx_cookies_accept input').click(
        function () {
            setTimeout(
                function () {
                    location.reload();
                },
                500);
        });

*/

//end document ready
});


//opt-out-link Datenschutzerklärung bei Google

// GOOGLE OPT_OUT_COOKIE  Set to the same value as the web property used on the site
var gaProperty = 'UA-XXXXXXX-X';

// Disable tracking if the opt-out cookie exists.
var disableStr = 'ga-disable-' + gaProperty;
if (document.cookie.indexOf(disableStr + '=true') > -1) {
    window[disableStr] = true;
}

// Opt-out function
function gaOptout() {
    document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
    window[disableStr] = true;

    //zazu.berlin addition to work with cookie Extension and set it through this link off
    document.cookie = 'tx_cookies_accepted=1; path=/';
    location.reload();
}

