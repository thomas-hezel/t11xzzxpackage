<?php
/**
 * Created by
 * User: thomas hezel
 * Date: 20190504 / 20220318
 *
 * Autoload for classes, this shows in the Backend e.g. version
 * ONLY NEEDED without COMPOSER OR TYPO3 below 11
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'Sitepackage by zazu.berlin',
    'description' => 'Sitepackage zazu.berlin',
    'category' => 'templates',
    'author' => 'Thomas Hezel',
    'author_email' => 'info@zazu.berlin',
    'author_company' => 'zazu.berlin',
    'version' => '2.1.0',
    'state' => 'stable',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'fluid_styled_content' => '11.5.0-11.5.99',
            'rte_ckeditor' => '11.5.0-11.5.99',
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Zazuberlin\\Xzzxpackage\\' => 'Classes',
        ],
    ],
    'clearCacheOnLoad' => 1
];
