<?php
defined('TYPO3') or die('Access denied.');

/* last changes: 20220317 */

call_user_func(function()
{
    /**
     * Temporary variables
     */
    $extensionKey = 'xzzxpackage';

    /**
     * Default PageTS for Sitepackage, plus entry in ext_localconf.php
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
        $extensionKey,
        'Configuration/TsConfig/Page/All.tsconfig',
        'Sitepackage global call for pageTS'
    );
});
