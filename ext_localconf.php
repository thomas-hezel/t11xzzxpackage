<?php
defined('TYPO3') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['zazu'] = 'EXT:xzzxpackage/Configuration/RTE/zazu-settings-RTE.yaml';

/***************
 * PageTS
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:xzzxpackage/Configuration/TsConfig/Page/All.tsconfig">');
